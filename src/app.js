function getRandomValue(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

const app = Vue.createApp({
    data() {
        return {
            playerHealth: 100,
            monsterHealth: 100,
            currentRound: 0,
            winner: null,
            logMessages: []
        }
    },
    computed: {
        specialAttackDisabled() {
            return (this.currentRound % 3 !== 0) || (this.currentRound === 0);
        },
        monsterHealthBarStyle() {
            return {
                width: this.monsterHealth <= 0 ? '0%' : this.monsterHealth + '%'
            }
        },
        playerHealthBarStyle() {
            return {
                width: this.playerHealth <= 0 ? '0%' : this.playerHealth + '%'
            }
        },
    },
    watch: {
        playerHealth(value) {
            if (value <= 0 && this.monsterHealth <= 0) {
                this.winner = 'draw';
            } else if (value <= 0) {
                this.winner = 'monster';
            }
        },
        monsterHealth(value) {
            if (value <= 0 && this.playerHealth <= 0) {
                this.winner = 'draw';
            } else if (value <= 0) {
                this.winner = 'player';
            }
        }
    },
    methods: {
        startNewGame() {
            this.playerHealth = 100;
            this.monsterHealth = 100;
            this.currentRound = 0;
            this.winner = null;
            this.logMessages = [];
        },
        submitRound() {
            this.currentRound++;
        },
        attackMonster() {
            this.submitRound();
            const attackValue = getRandomValue(5, 12);
            this.monsterHealth -= attackValue;
            this.addLogMessage('Player', 'Attack', attackValue);
            this.attackPlayer();
        },
        attackPlayer() {
            const attackValue = getRandomValue(8, 15);
            this.playerHealth -= attackValue;
            this.addLogMessage('Monster', 'Attack', attackValue);
        },
        specialAttackMonster() {
            this.submitRound();
            const attackValue = getRandomValue(10, 25);
            this.monsterHealth -= attackValue;
            this.addLogMessage('Player', 'Special Attack', attackValue);
            this.attackPlayer();
        },
        healPlayer() {
            this.submitRound();
            const healValue = getRandomValue(8, 20);
            if (this.playerHealth + healValue > 100) {
                this.playerHealth = 100;
            } else {
                this.playerHealth += healValue;
            }
            this.addLogMessage('Player', 'Heal', healValue);
            this.attackPlayer();
        },
        surrender() {
            this.winner = 'monster';
        },
        addLogMessage(who, what, value) {
            const message = {
                actionBy: who,
                actionType: what,
                actionValue: value
            }
            this.logMessages.unshift(message);
        },
        setLogMessageStyle(message) {
            const actionBy = message.actionBy;
            const actionType = message.actionType;

            let style = {};
            if (actionType === 'Heal') {
                style['color'] = 'green';
                return style;
            }

            if (actionBy === 'Player') {
                style['color'] = 'blue';
            } else if (actionBy === 'Monster') {
                style['color'] = 'red';
            }
            return style;
        },
    }
});

app.mount('#game');